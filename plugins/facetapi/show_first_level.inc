<?php

/**
 * Plugin that removes all Child items in the build.
 */
class FacetapiFilterFirstLevelTerm extends FacetapiFilter {

  // Filtered build.
  protected $build;
  protected $buildHierarchy;
  protected $facet;
  protected $globalSettings;

  /**
   * Filters items.
   */
  public function execute(array $build) {
	
    $this->buildHierarchy = $this->build = array();
    $this->facet = $this->adapter->getFacet(array('name' => $this->settings->facet));
    $this->globalSettings = $this->facet->getSettings();
    $this->buildHierarchy = $this->processHierarchy($build);

    foreach($this->buildHierarchy as $key => $item) {
	    if (!isset($this->buildHierarchy[$key]['#has_parents'])) {
		  $this->build[$key] = $build[$key]; 
	  }
  }
		
    return $this->build;
  }
	
  protected function processHierarchy(array $build) {

    // Builds the hierarchy information if the hierarchy callback is defined.
    if ($this->facet['hierarchy callback']) {
      $parents = $this->facet['hierarchy callback'](array_keys($build));
      foreach ($parents as $value => $parents) {
        if (isset($build[$value])) {
          $build[$value]['#has_parents'] = 1;
        }
	    }
    }

    return $build;
  }
	
}